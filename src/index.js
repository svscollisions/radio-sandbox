import anime from "animejs/lib/anime.es.js";
import * as dat from "dat.gui";
import * as Stats from "stats.js";
import {
  AudioListener,
  AudioLoader,
  Clock,
  Color,
  DirectionalLight,
  DirectionalLightHelper,
  FogExp2,
  HemisphereLight,
  Mesh,
  MeshPhongMaterial,
  PerspectiveCamera,
  PositionalAudio,
  Raycaster,
  Scene,
  SphereGeometry,
  WebGLRenderer,
  BoxGeometry,
  MeshBasicMaterial,
} from "three";
import * as THREE from "three";
import { PointerLockControls } from "three/examples/jsm/controls/PointerLockControls.js";
import { PositionalAudioHelper } from "three/examples/jsm/helpers/PositionalAudioHelper";
import * as Tone from "tone";

import { lerp, map, params, getDistance } from "./helpers.js";
import { lerpp, makeKalimba } from "./instruments";
import fragmentShader from "./shaders/fragment.glsl";
import vertexShader from "./shaders/vertex.glsl";
import { RectAreaLightUniformsLib } from "./lights/RectAreaLightUniformsLib.js";
import { songs } from "./data.js";

const colors = {
  primary: 0x141414,
  secondary: 0xacacd7,
  tertiary: 0xe5e4f4,
  transparent: 0x00000000,
};

const textOptions = {
  size: 80,
  height: 5,
  curveSegments: 12,
  bevelEnabled: true,
  bevelThickness: 10,
  bevelSize: 8,
  bevelOffset: 0,
  bevelSegments: 5,
};

const PLAY_SEQUENCE = true;

//REMOVE this in production
const DEBUG = true; // Set to false in production

if (DEBUG) {
  window.THREE = THREE;
}

let container, scene, camera, renderer, controls, gui;
let material1, material2, material3, material4;
let mesh1, mesh2, mesh3;
let audioLoader;
let listener;
let time, clock;
let stats;

let raycaster;
let objects;
let moveForward = false;
let moveBackward = false;
let moveLeft = false;
let moveRight = false;
let canJump = false;

const velocity = new THREE.Vector3();
const direction = new THREE.Vector3();
const vertex = new THREE.Vector3();

let objectsToRaycast;
let kalimba;
let kalimbaSeq;
let delay;
let reverb;
let timer;

function init() {
  container = document.querySelector(".container");
  scene = new Scene();
  objects = [];
  listener = new AudioListener();
  audioLoader = new AudioLoader();
  clock = new Clock(true);
  time = 0;
  objectsToRaycast = [];

  createCamera();
  listener.setMasterVolume(1);
  camera.add(listener);
  createLights();
  createRenderer();
  createGeometries();
  createControls();
  // createGridHelper();
  createSequences();
  // initGui();

  if (DEBUG) {
    window.scene = scene;
    window.camera = camera;
    window.controls = controls;
    stats = Stats.default();
    document.body.appendChild(stats.dom);
  }

  renderer.setAnimationLoop(() => {
    stats.begin();
    update();
    renderer.render(scene, camera);
    stats.end();
  });
}

// function initGui() {
//   gui = new dat.GUI();
//   window.gui = gui;
//   document.querySelector(".dg").style.zIndex = 99; //fix dat.gui hidden
//   gui.add(params, "testParam", -1.000001, 1.000001).onChange(() => {
//     let sphere = scene.children.filter((child) => child.name == "sphere");
//     if (sphere && sphere.length) {
//       sphere = sphere[0];
//       anime({
//         targets: sphere.position,
//         x: params.testParam,
//         duration: 500,
//         easing: "easeInOutSine",
//         update: function () {
//           //any custom updates
//         },
//       });
//     }
//   });
// }

function createCamera() {
  camera = new PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );
  camera.position.x = 0;
  camera.position.y = 1;
  camera.position.z = 600;
}

function createLights() {
  scene.fog = new FogExp2(colors.primary, 0.015);
  const directionalLight = new DirectionalLight(colors.secondary);
  directionalLight.position.set(0, 0.5, 1).normalize();

  const directionalLightHelper = new DirectionalLightHelper(
    directionalLight,
    5
  );
  const hemisphereLight = new HemisphereLight(
    colors.secondary,
    colors.tertiary,
    3
  );
  // Area lights
  const rectLight1 = new THREE.RectAreaLight(0xff0000, 1, 2, 10);
  rectLight1.position.set(-1, 1, 1);

  const rectLight2 = new THREE.RectAreaLight(0x00ff00, 1, 2, 10);
  rectLight2.position.set(0, 1, 1);

  const rectLight3 = new THREE.RectAreaLight(0x0000ff, 1, 2, 10);
  rectLight3.position.set(1, 1, 1);
  scene.add(directionalLight, hemisphereLight, directionalLightHelper);
}

function createRenderer() {
  renderer = new WebGLRenderer({ antialias: true, alpha: true });
  // renderer.setClearColor(colors.transparent,0);
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  document.body.appendChild(renderer.domElement);
}

function createPlantSphereMesh(song) {
  // Place geometry in scene
  let sphere = new BoxGeometry(50, 50, 10);
  let mesh = new Mesh(sphere, getPlantMaterial(song.image));
  mesh.position.set(100 * plusOrMinus(), song.posY + 10, song.posX);
  mesh.rotation.y = THREE.Math.degToRad( 0 );
  scene.add(mesh);
  objects.push(mesh);
  // Add sound
  mesh = startPositionalSound(mesh, song.audio);

  return mesh;
}

function getPlantMaterial(image) {
  return (material1 = new THREE.MeshBasicMaterial({
    map: new THREE.TextureLoader().load(image),
  }));
}

function startPositionalSound(mesh, audioFile) {
  const coneInnerAngle = 180;
  const coneOuterAngle = 230;
  const coneOuterGain = 0.1;
  const range = 10;
  const loop = true;

  let sound = new PositionalAudio(listener);
  audioLoader.load(audioFile, function (buffer) {
    sound.setBuffer(buffer);
    sound.setRefDistance(range);
    sound.setDirectionalCone(coneInnerAngle, coneOuterAngle, coneOuterGain);
    const helper1 = new PositionalAudioHelper(sound, range);
    sound.add(helper1);
    sound.setLoop(loop);
    sound.play();
    mesh.add(sound);
    return mesh;
  });
}

////////////
///
function createGeometries() {
  const sphere = new SphereGeometry(20, 32, 16);
  // TODO: add looping sampled drum sounds
  // listener - set to mute so theres no sound until you click play

  // Place songs on scene
  songs.forEach((song) => {
    createPlantSphereMesh(song);
  });

  // Wall
  let start = Date.now();
  timer = setInterval(function () {
    createWall();
  }, 5000);
}
var plusOrMinus = () => Math.round(Math.random()) * 2 - 1;
let getRandomNumber = (int=100) => int * Math.random() * plusOrMinus()
function createWall(
  pos = {
    x: getRandomNumber(800),
    y: 50,
    z: getRandomNumber(500),
  },
  width = 100,
  height = 100,
  depth = 2
) {
  const wallGeometry = new BoxGeometry(width, height, depth);
  const wallMaterial = new MeshBasicMaterial({
    map: new THREE.TextureLoader().load("textures/boom.png"),
    color: colors.secondary,
    transparent: true,
    opacity: 0.5,
  });

  console.log(pos);
  const wall = new Mesh(wallGeometry, wallMaterial);
  wall.position.set(pos.x, pos.y, pos.z);

  scene.add(wall);
}

function createControls() {
  controls = new PointerLockControls(camera, document.body);

  const blocker = document.getElementById("blocker");
  const instructions = document.getElementById("instructions");
  const play = document.getElementById("play");

  play.addEventListener("click", function () {
    controls.lock();
    listener.setMasterVolume(1);
    if (PLAY_SEQUENCE) {
      Tone.Transport.start();
      Tone.Transport.bpm.value = 80;
      Tone.start();
    }
  });

  controls.addEventListener("lock", function () {
    instructions.style.display = "none";
    blocker.style.display = "none";
  });

  controls.addEventListener("unlock", function () {
    blocker.style.display = "flex";
    instructions.style.display = "";
    if (PLAY_SEQUENCE) Tone.Transport.stop();
    listener.setMasterVolume(0);
  });

  scene.add(controls.getObject());

  const onKeyDown = function (event) {
    switch (event.code) {
      case "ArrowUp":
      case "KeyW":
        moveForward = true;
        break;

      case "ArrowLeft":
      case "KeyA":
        moveLeft = true;
        break;

      case "ArrowDown":
      case "KeyS":
        moveBackward = true;
        break;

      case "ArrowRight":
      case "KeyD":
        moveRight = true;
        break;

      case "Space":
        if (canJump === true) velocity.y += 350;
        canJump = false;
        break;
    }
  };

  const onKeyUp = function (event) {
    switch (event.code) {
      case "ArrowUp":
      case "KeyW":
        moveForward = false;
        break;

      case "ArrowLeft":
      case "KeyA":
        moveLeft = false;
        break;

      case "ArrowDown":
      case "KeyS":
        moveBackward = false;
        break;

      case "ArrowRight":
      case "KeyD":
        moveRight = false;
        break;
    }
  };

  document.addEventListener("keydown", onKeyDown);
  document.addEventListener("keyup", onKeyUp);

  raycaster = new Raycaster(
    new THREE.Vector3(),
    new THREE.Vector3(0, -1, 0),
    0,
    10
  );
}

function createGridHelper() {
  const helper = new THREE.GridHelper(1000, 10, 0x444444, 0x444444);
  helper.position.y = 0;
  scene.add(helper);
}

function update() {
  time = clock.getDelta();

  if (controls.isLocked === true) {
    raycaster.ray.origin.copy(controls.getObject().position);
    raycaster.ray.origin.y -= 10;

    const intersections = raycaster.intersectObjects(objects);

    const onObject = intersections.length > 0;

    const delta = time;

    const myLocation = controls.getObject();

    const speedMul = 0.5;
    velocity.x -= velocity.x * 20.0 * delta * speedMul;
    velocity.z -= velocity.z * 10.0 * delta * speedMul;

    velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

    direction.z = Number(moveForward) - Number(moveBackward);
    direction.x = Number(moveRight) - Number(moveLeft);
    direction.normalize(); // this ensures consistent movements in all directions

    if (moveForward || moveBackward) velocity.z -= direction.z * 400.0 * delta;
    if (moveLeft || moveRight) velocity.x -= direction.x * 400.0 * delta;

    if (getDistance(myLocation, objects[0]) < 50) {
      // const rateMap = map(getDistance(myLocation, objects[0]), 20, 50, 1, 3);
      // kalimbaSeq.playbackRate = rateMap;
      // delay.delayTime.value = "16n";
      // delay.feedback.value = 0.3;
      // reverb.decay = 5;
      // kalimba.harmonicity.value = 1;

      if (onObject) {
        delay.feedback.value = 0;
        const events =
          "E5,D#5,E5,B4,G#4,B4,E4,F#4,E4,D#4,E4,B3,G#3,B3,E4,D#4,E4,E4,F#4,E4,G#4,E4,A4,E4,G#4,E4,F#4,E4,E4,E5,D#5,C#5,B4,E5,D#5,C#5,B4,A4,G#4,F#4,E4,E4,F#4,E4,G#4,E4,A4,E4,G#4,E4,F#4,E4,E4,E5,D#5,C#5,B4,E5,D#5,C#5,B4,A4,G#4,F#4,E4,E4,E4,E4,D#4,E4,E4,E4,F#4,E4,D#4,E4,E4,E4,G#4,E4,F#4,E4,G#4,E4,A4,E4,F#4,E4,G#4,E4,E4,E4,D#4,E4,E4,E4,F#4,E4,D#4,E4,E4,E4,G#4,E4,F#4,E4,G#4,E4,A4,E4".split(
            ","
          );
        kalimbaSeq.events = events;
      }
    } else if (getDistance(myLocation, objects[1]) < 50) {
      // // red circle
      // const rateMap = map(getDistance(myLocation, objects[1]), 20, 50, 0.5, 1);
      // kalimbaSeq.playbackRate = rateMap;
      // delay.delayTime.value = "3n";
      // delay.feedback.value = 0.5;
      // kalimba.harmonicity.value = 5;

      if (onObject) {
        delay.feedback.value = 0;
        const events =
          "Eb4,A4,Bb4,Eb4,A4,Bb4,C5,A4,Bb4,C5,A4,Bb4,Bb4,A4,G4,F4,Eb4,F4,Bb4,Ab4,G4,F4,Eb4,F4,Eb4,G4,Eb4,A4,Bb4,Eb4,A4,Bb4,C5,A4,Bb4,C5,D5,Bb4,D5,Eb5,D5,C5,Bb4,D5,D5,Eb5,D5,C5,Bb4,D5,Eb5,F5".split(
            ","
          );
        kalimbaSeq.events = events;
      }
    } else if (getDistance(myLocation, objects[2]) < 50) {
      // const rateMap = map(getDistance(myLocation, objects[2]), 20, 50, 0.1, 0.5);
      // kalimbaSeq.playbackRate = rateMap;
      // delay.delayTime.value = "8n";
      // delay.feedback.value = 0.1;
      // kalimba.harmonicity.value = 8;

      if (onObject) {
        delay.feedback.value = 0;
        kalimbaSeq.events = ["c2"];
      }
    }

    // TODO: trigger sound when you jump on top
    if (onObject === true) {
      velocity.y = Math.max(0, velocity.y);
      canJump = true;
      console.log("intersecting");
    }

    controls.moveRight(-velocity.x * delta);
    controls.moveForward(-velocity.z * delta);

    controls.getObject().position.y += velocity.y * delta; // new behavior

    if (controls.getObject().position.y < 10) {
      velocity.y = 0;
      controls.getObject().position.y = 10;

      canJump = true;
    }
  }
}

function createSequences() {
  kalimba = makeKalimba().instrument;
  kalimba.volume.value = -17;
  delay = makeKalimba().delay;
  reverb = makeKalimba().reverb;
  kalimbaSeq = makeKalimba().seq;
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}
window.addEventListener("resize", onWindowResize, false);

init();
