if (getDistance(myLocation, mesh1) < 50) {
    const rateMap = map(getDistance(myLocation, mesh1), 20, 50, 1, 3);
    kalimbaSeq.playbackRate = rateMap;
    delay.delayTime.value = "16n";
    delay.feedback.value = 0.3;
    kalimba.harmonicity.value = 1;

    if (onObject) {
      delay.feedback.value = 0;
      const events =
        "E5,D#5,E5,B4,G#4,B4,E4,F#4,E4,D#4,E4,B3,G#3,B3,E4,D#4,E4,E4,F#4,E4,G#4,E4,A4,E4,G#4,E4,F#4,E4,E4,E5,D#5,C#5,B4,E5,D#5,C#5,B4,A4,G#4,F#4,E4,E4,F#4,E4,G#4,E4,A4,E4,G#4,E4,F#4,E4,E4,E5,D#5,C#5,B4,E5,D#5,C#5,B4,A4,G#4,F#4,E4,E4,E4,E4,D#4,E4,E4,E4,F#4,E4,D#4,E4,E4,E4,G#4,E4,F#4,E4,G#4,E4,A4,E4,F#4,E4,G#4,E4,E4,E4,D#4,E4,E4,E4,F#4,E4,D#4,E4,E4,E4,G#4,E4,F#4,E4,G#4,E4,A4,E4".split(
          ","
        );
      kalimbaSeq.events = events;
    }
  } else if (getDistance(myLocation, mesh2) < 50) {
    // red circle
    const rateMap = map(getDistance(myLocation, mesh2), 20, 50, 0.5, 1);
    kalimbaSeq.playbackRate = rateMap;
    delay.delayTime.value = "3n";
    delay.feedback.value = 0.5;
    kalimba.harmonicity.value = 5;

    if (onObject) {
      delay.feedback.value = 0;
      const events =
        "Eb4,A4,Bb4,Eb4,A4,Bb4,C5,A4,Bb4,C5,A4,Bb4,Bb4,A4,G4,F4,Eb4,F4,Bb4,Ab4,G4,F4,Eb4,F4,Eb4,G4,Eb4,A4,Bb4,Eb4,A4,Bb4,C5,A4,Bb4,C5,D5,Bb4,D5,Eb5,D5,C5,Bb4,D5,D5,Eb5,D5,C5,Bb4,D5,Eb5,F5".split(
          ","
        );
      kalimbaSeq.events = events;
    }
  } else if (getDistance(myLocation, mesh3) < 50) {
    const rateMap = map(getDistance(myLocation, mesh3), 20, 50, 0.1, 0.5);
    kalimbaSeq.playbackRate = rateMap;
    delay.delayTime.value = "8n";
    delay.feedback.value = 0.1;
    kalimba.harmonicity.value = 8;

    if (onObject) {
      delay.feedback.value = 0;
      kalimbaSeq.events = ["c2"];
    }
  }