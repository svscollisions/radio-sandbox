import * as Tone from "tone";

export function makeKalimba() {
  var panner = new Tone.Panner3D().toDestination();
  var instrument = new Tone.FMSynth();
  var kalimbaOptions = {
    harmonicity: 8,
    modulationIndex: 2,
    oscillator: {
      type: "sine",
    },
    envelope: {
      attack: 0.001,
      decay: 6,
      sustain: 0,
      release: 0.1,
    },
    modulation: {
      type: "sine",
    },
    modulationEnvelope: {
      attack: 0.002,
      decay: 0.2,
      sustain: 1,
      release: 0.1,
    },
  };

  const celloOptions = {
    harmonicity: 3.01,
    modulationIndex: 14,
    oscillator: {
      type: "triangle",
    },
    envelope: {
      attack: 0,
      decay: .0001,
      sustain: 0.001,
      release: 0,
    },
    modulation: {
      type: "square",
    },
    modulationEnvelope: {
      attack: 0,
      decay: 0.01,
      sustain: 0.001,
      release: 0,
    },
  };

  instrument.set(celloOptions);
  const vol = new Tone.Volume(-100).toDestination();
  
  var effect1, effect2, effect3;
  
  // create effects
  var effect1 = new Tone.PingPongDelay();
  effect1.set({
      delayTime: "8n",
      feedback: .8,
      wet: 0.5,
    });
  var effect2 = new Tone.Reverb();
  effect2.set({
    decay: 1,
    preDelay: 1,
  });

  const filter = new Tone.AutoFilter(4).start();
  const distortion = new Tone.Distortion(0.5);
    
  // make connections
  instrument.connect(vol);
  instrument.connect(effect2);
  instrument.connect(filter);
  instrument.connect(distortion);
  // instrument.connect(effect1);
  // effect1.connect(panner);
  effect2.connect(panner);

  const seq = new Tone.Sequence(
    (time, note) => {
      instrument.triggerAttackRelease(note, 8/Math.random(), time);
      // subdivisions are given as subarrays
    },
    ["C1", ["D1", "D1", "E1"], "D1", ["A1", "G1"]]
  ).start(0);
  // Tone.Transport.start();

  // define deep dispose function
  function deep_dispose() {
    if (effect1 != undefined && effect1 != null) {
      effect1.dispose();
      effect1 = null;
    }
    if (effect2 != undefined && effect2 != null) {
      effect2.dispose();
      effect2 = null;
    }
    if (effect3 != undefined && effect3 != null) {
      effect3.dispose();
      effect3 = null;
    }
    if (instrument != undefined && instrument != null) {
      instrument.dispose();
      instrument = null;
    }
  }

  // TODO: add sequencer from guitarland
  return {
    instrument: instrument,
    delay: effect1,
    reverb: effect2,
    deep_dispose: deep_dispose,
    seq,
  };
}
